lily-spawni
===========

[View Documentation](https://fascinatedbox.gitlab.io/lily-spawni/spawni/module.spawni.html)

Provide an interpreter as a value, so you can interpret while you interpret. One
use of this is [providing a repl](https://gitlab.com/fascinatedbox/lily-repl).
This can be installed using Lily's `garden` via:

`garden install FascinatedBox/lily-spawni`
